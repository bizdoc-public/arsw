using System;
using System.Collections.Generic;
using System.Linq;

namespace tema_arsw
{
    public class Graph
    {
        public IDictionary<Vertex, List<Edge>> AdjList { get; set; }

        public Graph()
        {
            AdjList = new Dictionary<Vertex, List<Edge>>();
        }

        public void AddVertex(Vertex x)
        {
            if (!AdjList.ContainsKey(x))
            {
                AdjList.Add(x, new List<Edge>());
            }
        }

        public void AddEdge(Vertex start, Vertex end, bool oriented = false)
        {
            if (!AdjList.ContainsKey(start))
            {
                throw new Exception($"{start} vertex does not exists");
            }

            if (!AdjList.ContainsKey(end))
            {
                throw new Exception($"{end} vertex does not exists");
            }

            AdjList[start].Add(new Edge(end));
            if (!oriented)
                AdjList[end].Add(new Edge(start));
        }

        public void AddEdge(Vertex start, Vertex end, int weight, bool oriented = false)
        {
            AdjList[start].Add(new Edge(end, weight));
            if (!oriented)
                AdjList[end].Add(new Edge(start, weight));
        }

        public override string ToString()
        {
            var result = "";
            foreach (var adjListKey in AdjList.Keys)
            {
                var tmp = "";
                foreach (var vertex in AdjList[adjListKey])
                {
                    tmp += vertex.Vertex.Name + " ";
                }

                tmp = adjListKey.Name + " -> " + tmp + "\n";
                result += tmp;
            }

            return result;
        }

        public IEnumerable<Edge> Path(Vertex start, Vertex end)
        {
            var weights = new Dictionary<Vertex, int>();
            var checks = new HashSet<Vertex>();

            foreach (var adjListKey in AdjList.Keys)
            {
                weights.Add(adjListKey, Int32.MaxValue);
            }

            weights[start] = 0;
            UpdateWeights(weights, checks, start, end);
            return GetPath(start, end, weights);
        }

        private void UpdateWeights(IDictionary<Vertex, int> weights, HashSet<Vertex> checks, Vertex start, Vertex end)
        {
            foreach (var edge in AdjList[start])
            {
                if (weights[start] + edge.Weight < weights[edge.Vertex])
                {
                    weights[edge.Vertex] = weights[start] + edge.Weight;
                }
            }

            checks.Add(start);
            foreach (var edge in AdjList[start])
            {
                if (!checks.Contains(edge.Vertex))
                {
                    UpdateWeights(weights, checks, edge.Vertex, end);
                }
            }
        }

        private IEnumerable<Edge> GetPath(Vertex start, Vertex end, IDictionary<Vertex, int> weights)
        {
            if (!Equals(start, end))
            {
                var e = AdjList[end].FirstOrDefault(a => a.Weight == weights[end] - weights[a.Vertex]);
                if (e is null)
                {
                    yield break;
                }
                var arr = GetPath(start, e.Vertex, weights);
                foreach (var edge in arr)
                {
                    yield return edge;
                }

                yield return e;
            }
        }

        public static string PathToString(IEnumerable<Edge> path)
        {
            var result = "";
            foreach (var vertex in path)
            {
                result += vertex.Vertex + " {" + $"{vertex.Weight}" + "} ";
            }

            if (string.IsNullOrWhiteSpace(result))
            {
                return "Cant find a path";
            }
            return result;
        }

        public List<List<Vertex>> GetConexComponents()
        {
            var checks = new HashSet<Vertex>();
            var result = new List<List<Vertex>>();
            foreach (var adjListKey in AdjList.Keys)
            {
                if (checks.Contains(adjListKey)) continue;
                var subhraph = new List<Vertex>();
                checks.Add(adjListKey);
                subhraph.Add(adjListKey);
                FindComponents(checks, subhraph, adjListKey);
                result.Add(subhraph);
            }

            return result;
        }
        private void FindComponents(HashSet<Vertex> checks, List<Vertex> subhraph, Vertex current)
        {
            foreach (var edge in AdjList[current])
            {
                if (!checks.Contains(edge.Vertex))
                {
                    checks.Add(edge.Vertex);
                    subhraph.Add(edge.Vertex);
                    FindComponents(checks, subhraph, edge.Vertex);
                }
            }
        }
    }
}