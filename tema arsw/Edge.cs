namespace tema_arsw
{
    public class Edge
    {
        public Vertex Vertex { get; set; }
        public int Weight { get; set; } = 1;

        public Edge(Vertex v)
        {
            Vertex = v;
        }

        public Edge(Vertex v, int weight)
        {
            Vertex = v;
            Weight = weight;
        }
    }
}