using System.Collections.Generic;

namespace tema_arsw
{
    public class Vertex
    {
        public string Name { get; set; }

        public Vertex()
        {
        }

        public Vertex(string name)
        {
            Name = name;
        }
        public Vertex(int name)
        {
            Name = name.ToString();
        }

        public override bool Equals(object? obj)
        {
            return Name.Equals((obj as Vertex)?.Name);
        }

        protected bool Equals(Vertex other)
        {
            return Name == other.Name;
        }

        public override int GetHashCode()
        {
            return (Name != null ? Name.GetHashCode() : 0);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}