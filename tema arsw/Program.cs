﻿using System;
using System.Linq;

namespace tema_arsw
{
    class Program
    {
        static void Main(string[] args)
        {
            // Graph g = init();
            // var path = g.Path(new Vertex("A"), new Vertex("F"));
            // Console.WriteLine(Graph.PathToString(path));
            Test1();
            Test2();
        }

        static void Test2()
        {
            var graph = new Graph();
            graph.AddVertex(new Vertex("A"));
            graph.AddVertex(new Vertex("B"));
            graph.AddVertex(new Vertex("C"));
            graph.AddVertex(new Vertex("D"));
            graph.AddVertex(new Vertex("E"));
            graph.AddVertex(new Vertex("F"));

            graph.AddEdge(new Vertex("A"), new Vertex("B"));
            graph.AddEdge(new Vertex("A"), new Vertex("D"));
            graph.AddEdge(new Vertex("A"), new Vertex("E"));
            graph.AddEdge(new Vertex("B"), new Vertex("C"));
            graph.AddEdge(new Vertex("D"), new Vertex("E"));
            graph.AddEdge(new Vertex("E"), new Vertex("F"));
            graph.AddEdge(new Vertex("E"), new Vertex("C"));
            graph.AddEdge(new Vertex("C"), new Vertex("F"));

            var path = graph.Path(new Vertex("A"), new Vertex("F"));
            Console.WriteLine(Graph.PathToString(path));
        }

        static void Test1()
        {
            //https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
            var g = new Graph();
            g.AddVertex(new Vertex("1"));
            g.AddVertex(new Vertex("2"));
            g.AddVertex(new Vertex("3"));
            g.AddVertex(new Vertex("4"));
            g.AddVertex(new Vertex("5"));
            g.AddVertex(new Vertex("6"));
            
            g.AddVertex(new Vertex("10"));
            g.AddVertex(new Vertex("9"));

            g.AddEdge(new Vertex(1), new Vertex(2), 7);
            g.AddEdge(new Vertex(1), new Vertex(3), 9);
            g.AddEdge(new Vertex(1), new Vertex(6), 14);

            g.AddEdge(new Vertex(2), new Vertex(3), 10);
            g.AddEdge(new Vertex(2), new Vertex(4), 15);

            g.AddEdge(new Vertex(3), new Vertex(6), 2);
            g.AddEdge(new Vertex(3), new Vertex(4), 11);

            g.AddEdge(new Vertex(4), new Vertex(5), 6);

            g.AddEdge(new Vertex(6), new Vertex(5), 9);
            
            g.AddEdge(new Vertex(10), new Vertex(9), -1);

            var path = g.Path(new Vertex(1), new Vertex(5));
            var pathString = Graph.PathToString(path);
            Console.WriteLine(pathString);
            
            var components = g.GetConexComponents();
            Console.WriteLine($"number of components: {components.Count}");
            foreach (var component in components)
            {
                Console.Write("\t");
                foreach (var vertex in component)
                {
                    Console.Write(vertex.Name + " ");
                }
                Console.WriteLine();
            }
            
            var path2 = g.Path(new Vertex(1), new Vertex(10));
            var pathString2 = Graph.PathToString(path2);
            Console.WriteLine(pathString2);
            
        }
    }
}